FactoryGirl.define do
  factory :cash_machine do
    title { Faker::Job.title }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }

    trait :with_moscow_position do
      latitude 55.852869
      longitude 37.447015
    end
  end
end
