require 'rails_helper'

RSpec.describe CashMachinesController, type: :controller do
  describe 'GET .index' do
    context 'when ATMs exist' do
      let(:atm) { create :cash_machine }

      before do
        get :index
      end

      it 'responds with a 200 status' do
        assert_response 200
      end

      it 'populates an array of cash machines' do
        assigns(:cash_machines).should eq([atm])
      end

      it 'renders the :index view' do
        response.should render_template :index
      end
    end
  end

  describe '.new' do
    before do
      get :new
    end

    it 'renders the :new view' do
      response.should render_template :new
    end
  end

  describe '.show' do
    context 'when record exists' do
      let(:atm) { create :cash_machine }

      before do
        params = {id: atm.id}
        get :show, params: params
      end

      it 'responds with a 200 status' do
        assert_response 200
      end

      it 'renders the :index view' do
        response.should render_template :show
      end
    end
  end

  describe '.create' do
    let(:atm) { create :cash_machine }

    context 'when attributes valid' do
      before do
        cash_machine = atm.attributes
        params = {}
        params.store(:cash_machine, cash_machine)
        get :create, params: params
      end

      it 'responds with a 302-redirect status' do
        assert_response 302
      end

      it 'increase created cash machines by 1' do
        expect(CashMachine.count).to eq 2
      end

      it 'redirect_to root path' do
        response.should redirect_to :root
      end
    end

    context 'when attributes are not valid' do
      before do
        cash_machine = atm.attributes.merge(longitude: 'damn it')
        params = {}
        params.store(:cash_machine, cash_machine)
        get :create, params: params
      end

      it 'responds with a 200 status' do
        assert_response 200
      end

      it 'renders the :index view' do
        response.should render_template :new
      end
    end
  end

  describe '.find' do
    context 'when ATMs is nearby' do
      let!(:atm) { create :cash_machine, :with_moscow_position }

      before do
        cash_machine = {latitude: 55.86, longitude: 37.49}
        params = {}
        params.store(:cash_machine, cash_machine)
        get :find, params: params
      end

      it 'responds with a 200 status' do
        assert_response 200
      end

      it 'return nearest ATM' do
        expect(CashMachine.near([55.86, 37.49], 50, units: :km)[0]).to eq atm
      end

      it 'renders the :find view' do
        response.should render_template :find
      end
    end
  end

  describe '.destroy' do
    context 'when record deleted with success' do
      let(:atm) { create :cash_machine }

      before do
        params = {id: atm.id}
        get :destroy, params: params
      end

      it 'responds with a 200 status' do
        assert_response 302
      end

      it 'populates an array of cash machines' do
        expect(CashMachine.all).to eq []
      end

      it 'renders the :index view' do
        response.should redirect_to :root
      end
    end
  end
end
