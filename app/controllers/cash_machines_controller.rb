# frozen_string_literal: true

class CashMachinesController < ApplicationController
  before_action :set_location, only: [:destroy, :show]

  def index
    @cash_machines = CashMachine.all
  end

  def new
    @cash_machine = CashMachine.new
  end

  def show; end

  def create
    @cash_machine = CashMachine.new(location_params)
    if @cash_machine.save
      flash[:success] = 'ATM added!'
      redirect_to root_path
    else
      render 'new'
    end
  end

  def find
    @cash_machines = CashMachine.near([location_params[:latitude], location_params[:longitude]], 50, units: :km)
                                .limit(5)
  end

  def destroy
    @cash_machine.destroy
    redirect_to root_path
  end

  private

  def set_location
    @cash_machine = CashMachine.find(params[:id])
  end

  def location_params
    params.require(:cash_machine).permit(:title, :latitude, :longitude)
  end
end
