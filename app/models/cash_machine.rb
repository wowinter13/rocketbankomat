class CashMachine < ApplicationRecord
  validates :longitude, numericality: true
  validates :latitude, numericality: true

  reverse_geocoded_by :latitude, :longitude
  after_validation :reverse_geocode
end
