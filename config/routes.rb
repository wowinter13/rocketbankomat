Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # get 'list', to: 'cash_machines#all'
  # get 'result', to: 'cash_machines#find_nearest'
  # get 'chetotam', to: 'cash_machines#chetotam'

  resources :cash_machines, except: [:update, :edit] do
    post 'find', on: :collection
  end

  root 'cash_machines#index'

  match '/', to: 'cash_machines#index', via: 'get'
end
