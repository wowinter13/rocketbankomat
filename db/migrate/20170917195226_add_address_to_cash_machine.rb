class AddAddressToCashMachine < ActiveRecord::Migration[5.1]
  def change
    add_column :cash_machines, :address, :text
  end
end
