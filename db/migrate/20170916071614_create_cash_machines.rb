class CreateCashMachines < ActiveRecord::Migration[5.1]
  def change
    create_table :cash_machines do |t|
      t.string :title
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
